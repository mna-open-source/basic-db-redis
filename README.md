# Docker + Redis

## Prerequisite Software

Before you can work with this project, you must install and configure the following products on your development machine:

- [Git](http://git-scm.com) and/or the **GitHub app** (for [Mac](http://mac.github.com) or [Windows](http://windows.github.com))
- [Docker](https://docs.docker.com/engine/install/)

## Run Docker Compose

Run the docker container with following command:

```shell
docker-compose up
```

Validate access with browser at http://127.0.0.1:8081 or run redis cli with following command:


```shell
docker exec -it mna-cache redis-cli ping
redis-cli -p 7001 ping
```

To stop all services

```shell
docker-compose down
```


## Resources

- [Docker](https://www.docker.com/)
- [Redis](https://redis.io/)
- [Redis Commander](https://github.com/joeferner/redis-commander)
